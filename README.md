GNU Guix hacking
================

All source code in this git repository is released under the GNU General
Public License version 3 terms. All guides and documents are released under
the GNU Free Documentation License version 1.3 terms.

2014, Ernesto Angel Celis de la Fuente
